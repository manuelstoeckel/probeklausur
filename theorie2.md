## Aufgabe 6
Was berechnet die folgende Funktion?
Erläutern Sie möglichst allgemein/abstrakt, was die Funktion berechnet.

```go
func foo2(n, m int) int {
	if n < m {
		return 0
	}
	return 1 + foo2(n-m, m)
}
```

## Aufgabe 7
Welche der folgenden Zeilen sind korrekte Variablendefinitionen in Go?

  [] `var i1 int`  
  [] `var i2 int = 42`  
  [] `int i3 = 42`  
  [] `i4 := 42`  
  [] `i5 int = 42`  
  [] `var l1 []int = [1,3,5]`  
  [] `l2 := []int(1,3,5)`  
  [] `l3 := []int{1,3,5}`  

**Hinweis:** Bei dieser Art von Aufgaben gibt es pro korrekt angekreuzter Zeile einen Punkt.
             für jede falsch angekreuzte Zeile gibt es einen Punkt Abzug.

