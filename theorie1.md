# Probe-Theorie-Aufgaben

## Aufgabe 1

Die folgende Funktion sollte in einer Liste alle Elemente verdoppeln.
Sie compiliert nicht und würde so auch nicht richtig funktionieren.
Erläutern Sie die Fehler.

```go
func doubleAll([]int list) {
    for i,v := range l {
        l[i] * 2
    }
}

```

## Aufgabe 2

Im folgenden wird ein Struct `Person` mit einer Methode `setBirthday()` definiert.
Der Aufruf `p1.setBirthday()` in `main()` sollte dazu führen, dass anschließend
Tag, Monat und Jahr korrekt in Person eingetragen sind. Das funktioniert aber nicht.
Erläutern Sie den Fehler.

```go
type Person struct {
    name string
    day, month, year int
}

func (p Person) setBirthday(day, month, year int) {
    p.day, p.month, p.year = day, month, year
}

func main() {
  p1 := Person{name: "Donald Knuth"}
  p1.setBirthday(10,1,1938)
  
  fmt.Println(p1)
}
```

## Aufgabe 3
Was berechnet die folgende Funktion:
Erläutern Sie möglichst allgemein/abstrakt, was die Funktion berechnet.

```go
func foo1(n, m int) int {
	if n >= m {
		return foo1(n-m, m)
	}
	if n < 0 {
		return foo1(n+m, m)
	}
	return n
}
```

## Aufgabe 4
Entwerfen Sie einen Datentyp (ein `struct`) für Personen in einem sozialen Netzwerk.
Der Datentyp soll geeignet sein, Metadaten zu einer Person zu speichern.
Dies umfasst Daten wie den Namen, das Geburtsdatum etc, aber auch Informationen
dazu, mit wem die Person befreundet ist und mit wem sie wie oft kommuniziert.

Erläutern Sie Ihren Vorschlag. Erklären Sie insbesondere die Wahl der Datentypen
für die Felder Ihres Structs.

**Hinweis:**
Sie sollten Go-Syntax für diese Aufgabe verwenden, müssen aber keinen lauffähigen
Go-Code produzieren. D.h. kleinere Fehler oder Unvollständigkeiten sind kein Problem.

## Aufgabe 5
Was berechnet die folgende Funktion:
Erläutern Sie möglichst allgemein/abstrakt, was die Funktion berechnet.

```go
func bar1(list []int, x int) ([]int, []int) {
	r1 := make([]int, 0)
	r2 := make([]int, 0)

	for _, v := range list {
		if v < x {
			r1 = append(r1, v)
		} else {
			r2 = append(r2, v)
		}
	}

	return r1, r2
}
```
