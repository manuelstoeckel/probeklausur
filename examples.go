package main

import "fmt"

func main() {
	ExampleFoo1()
	ExampleBar1()
	ExampleFoo2()

}

func ExampleFoo1() {
	fmt.Println(foo1(5, 3))
	fmt.Println(foo1(2, 3))
	fmt.Println(foo1(15, 3))
	fmt.Println(foo1(7, 2))
}

func ExampleBar1() {
	fmt.Println(bar1([]int{1, 13, 2, 25, 12, 42}, 15))
}

func ExampleFoo2() {
	fmt.Println(foo2(5, 3))
	fmt.Println(foo2(2, 3))
	fmt.Println(foo2(17, 3))
}

func ExampleDefinitions() {
	/*
		var i1 int
		var i2 int = 42
		int i3 = 42
		i4 := 42
		i5 int = 42
		var l1 []int = [1,3,5]
		l2 := []int(1,3,5)
		l3 := []int{1,3,5}

		// Alle einmal ausgeben, damit es compiliert.
		fmt.Println(i1, i2, i3, i4, i5, l1, l2, l3)
	*/
}

func foo1(n, m int) int {
	if n >= m {
		return foo1(n-m, m)
	}
	if n < 0 {
		return foo1(n+m, m)
	}
	return n
}

func bar1(list []int, x int) ([]int, []int) {
	r1 := make([]int, 0)
	r2 := make([]int, 0)

	for _, v := range list {
		if v < x {
			r1 = append(r1, v)
		} else {
			r2 = append(r2, v)
		}
	}

	return r1, r2
}

func foo2(n, m int) int {
	if n < m {
		return 0
	}
	return 1 + foo2(n-m, m)
}
