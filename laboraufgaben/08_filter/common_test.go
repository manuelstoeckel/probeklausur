package main

import "fmt"

// Gibt Ergebnisse von sums() auf die Konsole aus.
// Der Kommentar unten gibt die erwarteten Ergebnisse an.
// Automatische Prüfung mittels des Befehls "go test" (statt "go run").
func ExampleFilter() {
	l1 := []int{1, 3, 5, 7, 9, 11}
	l2 := []int{2, 4, 6, 8, 10, 12}
	l3 := []int{3, 6, 9, 12, 15, 18}

	fmt.Println(common(l1, l2))
	fmt.Println(common(l1, l3))
	fmt.Println(common(l2, l3))

	// Output:
	// []
	// [3 9]
	// [6 12]
}
